Watch Face Constructor Library
==============================

## About

This project is builded for creating watch faces quickly.

<h2>Source</h2>

The source code in this repository reflects the watch face constructor library.

<h2>Features</h2>

- Common library for common tasks for both companion and wear apps
- Mobile library
- Wear library

<h2>Copyright</h2>

    Copyright 2016 Peppy Works. All rights reserved.
ent