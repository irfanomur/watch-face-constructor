/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.watchfaceconstructor.wearable;

import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Wearable;
import com.google.android.gms.wearable.WearableListenerService;
import com.peppyworks.watchfaceconstructor.common.Constants;
import com.peppyworks.watchfaceconstructor.common.util.LogUtils;
import com.peppyworks.watchfaceconstructor.common.util.WearableAPIHelper;

import java.util.concurrent.TimeUnit;

/**
 * A {@link WearableListenerService} listening for {@link PeppyWatchFaceService} config
 * messages
 * and updating the config {@link com.google.android.gms.wearable.DataItem} accordingly.
 */
public class ConstructorCompanionListenerService extends WearableListenerService
        implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = LogUtils.makeLogTag("CompanionListener");

    private GoogleApiClient mGoogleApiClient;

    @Override // WearableListenerService
    public void onMessageReceived(MessageEvent messageEvent) {

        LogUtils.LOGD(TAG, "onMessageReceived: " + messageEvent);

        if (messageEvent.getPath().equals(Constants.PATH_CONFIG)) {
            byte[] rawData = messageEvent.getData();
            // It's allowed that the message carries only some of the keys used in the DataItem
            // and skips the ones that we don't want to change.
            DataMap configKeysToOverwrite = DataMap.fromByteArray(rawData);
            LogUtils.LOGD(TAG, "Received watch face config message: " + configKeysToOverwrite);

            if (!checkGoogleClient()) {
                return;
            }

            WearableAPIHelper.overwriteKeysInDataMap(Constants.PATH_CONFIG,
                    mGoogleApiClient,
                    configKeysToOverwrite);
        } else if (messageEvent.getPath().equals(Constants.PATH_REQUEST_PHONE_BATTERY_LEVEL)) {
            byte[] rawData = messageEvent.getData();
            DataMap batteryLevelKeyToOverwrite = DataMap.fromByteArray(rawData);
            LogUtils.LOGD(TAG, "Received phone battery level message: " + batteryLevelKeyToOverwrite);

            if (!checkGoogleClient()) {
                return;
            }

            WearableAPIHelper.overwriteKeysInDataMap(Constants.PATH_REQUEST_PHONE_BATTERY_LEVEL,
                    mGoogleApiClient,
                    batteryLevelKeyToOverwrite);
        }
    }

    private boolean checkGoogleClient() {
        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).addApi(Wearable.API).build();
        }
        if (!mGoogleApiClient.isConnected()) {
            ConnectionResult connectionResult =
                    mGoogleApiClient.blockingConnect(30, TimeUnit.SECONDS);

            if (!connectionResult.isSuccess()) {
                LogUtils.LOGE(TAG, "Failed to connect to GoogleApiClient.");
                return false;
            }
        }

        return true;
    }

    @Override // GoogleApiClient.ConnectionCallbacks
    public void onConnected(Bundle connectionHint) {
        LogUtils.LOGD(TAG, "onConnected: " + connectionHint);
    }

    @Override  // GoogleApiClient.ConnectionCallbacks
    public void onConnectionSuspended(int cause) {
        LogUtils.LOGD(TAG, "onConnectionSuspended: " + cause);
    }

    @Override  // GoogleApiClient.OnConnectionFailedListener
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        LogUtils.LOGD(TAG, "onConnectionFailed: " + result);
    }

}
