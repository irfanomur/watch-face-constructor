/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.watchfaceconstructor.wearable;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.os.BatteryManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.annotation.NonNull;
import android.support.wearable.watchface.CanvasWatchFaceService;
import android.support.wearable.watchface.WatchFaceService;
import android.support.wearable.watchface.WatchFaceStyle;
import android.text.format.DateFormat;
import android.text.format.DateUtils;
import android.view.SurfaceHolder;
import android.view.WindowInsets;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.fitness.Fitness;
import com.google.android.gms.fitness.FitnessStatusCodes;
import com.google.android.gms.fitness.data.DataPoint;
import com.google.android.gms.fitness.data.DataType;
import com.google.android.gms.fitness.data.Field;
import com.google.android.gms.fitness.result.DailyTotalResult;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataEvent;
import com.google.android.gms.wearable.DataEventBuffer;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.peppyworks.watchfaceconstructor.common.BaseWatchFaceConfig;
import com.peppyworks.watchfaceconstructor.common.BaseWatchFaceDrawer;
import com.peppyworks.watchfaceconstructor.common.Constants;
import com.peppyworks.watchfaceconstructor.common.WatchFaceCustomization;
import com.peppyworks.watchfaceconstructor.common.WatchMode;
import com.peppyworks.watchfaceconstructor.common.WatchShape;
import com.peppyworks.watchfaceconstructor.common.util.LogUtils;
import com.peppyworks.watchfaceconstructor.common.util.WatchFaceUtil;
import com.peppyworks.watchfaceconstructor.common.util.WearableAPIHelper;

import java.util.Calendar;
import java.util.List;
import java.util.Locale;
import java.util.TimeZone;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public abstract class PeppyWatchFaceService extends CanvasWatchFaceService {
    private static final String TAG = "PeppyWatchFaceService";

    private final WatchFaceEngine mWatchFaceEngine = new WatchFaceEngine();
    private BaseWatchFaceDrawer mWatchFaceDrawer;
    private int mWidth;
    private int mHeight;
    int mChinSize;
    private Calendar mPreviousCalendar;
    private Calendar mLatestCalendar;
    private boolean mIsAmbient = false;
    private boolean mLowBitAmbient = false;
    private boolean mBurnInProtection = false;
    private boolean mIs24HourFormat = false;
    private WatchShape mWatchShape = WatchShape.UNKNOWN;
    private WindowInsets mFaceInsets;
    private boolean mLayoutComplete;
    private boolean mVisible;
    private int mWatchBatteryLevel;
    private int mPhoneBatteryLevel;
    private int mStepsTotal = 0;

    private GoogleApiClient mGoogleApiClient;
    private String mPeerId;

    private boolean mStepsRequested;

    private final Rect mPeekCardBounds = new Rect();

    private WatchFaceUtil.AmbientMode mConfiguredAmbientMode = WatchFaceUtil.AmbientMode.DEFAULT;
    private WatchFaceUtil.TimeFormat mConfiguredTimeFormat = WatchFaceUtil.TimeFormat.DEFAULT;

    /**
     * Returns the width of the watch face.
     *
     * @return value of the watch face {@link android.graphics.Rect}s width
     */
    public final int getWidth() {
        return mWidth;
    }

    /**
     * Returns the height of the watch face.
     *
     * @return value of the watch face {@link android.graphics.Rect}s height
     */
    public final int getHeight() {
        return mHeight;
    }

    public final int getChinSize() {
        return mChinSize;
    }

    /**
     * Return whether the wallpaper is currently visible to the user, this is the last value
     * supplied to {@link WatchFaceEngine#onVisibilityChanged(boolean)}.
     */
    public final boolean isVisible() {
        return mVisible;
    }

    /**
     * Override to provide a custom {@link android.support.wearable.watchface.WatchFaceStyle} for
     * the watch face.
     *
     * @return {@link android.support.wearable.watchface.WatchFaceStyle} for watch face.
     */
    protected WatchFaceStyle getWatchFaceStyle() {
        return null;
    }

    /**
     * Returns the {@link WatchMode#INTERACTIVE} mode update rate in millis. This will tell the
     * {@link PeppyWatchFaceService} base class the period to call {@link
     * #onCalendarChanged(Calendar, Calendar)}. <br><br>DEFAULT={@link
     * android.text.format.DateUtils#MINUTE_IN_MILLIS}
     *
     * @return number of millis to wait before calling onTimeChanged and onDraw.
     */
    protected long getInteractiveModeUpdateRate() {
        return DateUtils.MINUTE_IN_MILLIS;
    }

    /**
     * Called when the size and shape of the watch face are first realized, and then every time they
     * are changed.
     *
     * @param shape        the watch screen shape.
     * @param width        the raw screen width.
     * @param height       the raw screen height.
     * @param screenInsets the screen's window insets.
     */
    public void onLayout(WatchShape shape, int width, int height, WindowInsets screenInsets) {
        LogUtils.LOGV(TAG,
                String.format(
                        "BaseWatchFaceDrawer.onLayout: Shape=%s; Width=%s; Height=%s; Insets=%s",
                        shape.name(),
                        width,
                        height,
                        screenInsets));

        if (screenInsets != null) {
            mChinSize = screenInsets.getSystemWindowInsetBottom();
        }
    }

    /**
     * Lifecycle event guaranteed to be called once after {@link #onLayout(WatchShape, int, int,
     * WindowInsets)} has been called for the first time.
     */
    public void onLayoutCompleted() {
    }

    /**
     * Override to perform view and logic updates. This will be called once per minute ({@link
     * WatchFaceEngine#onTimeTick()}) in {@link WatchMode#AMBIENT} modes. This is also called when
     * the date, time, and/or time zone (ACTION_DATE_CHANGED, ACTION_TIME_CHANGED, and
     * ACTION_TIMEZONE_CHANGED intents, respectively) is changed on the watch.
     *
     * @param oldCalendar {@link Calendar} last time this method was called.
     * @param newCalendar updated {@link Calendar}
     */
    protected void onCalendarChanged(Calendar oldCalendar, Calendar newCalendar) {
        LogUtils.LOGV(TAG, String.format(Locale.getDefault(),
                "WatchFace.onCalendarChanged: oldCalendar=%d; newCalendar=%d",
                oldCalendar.getTimeInMillis(), newCalendar.getTimeInMillis()));
    }

    protected void registerWatchFaceDrawer(BaseWatchFaceDrawer watchFaceDrawer) {
        mWatchFaceDrawer = watchFaceDrawer;
        mWatchFaceDrawer.setWatchFaceCustomization(mWatchFaceEngine);
        mWatchFaceDrawer.setBaseWatchFaceConfig(mWatchFaceEngine);
    }

    /**
     * Called when the system tells us the current watch mode has changed (e.g. {@link
     * WatchFaceEngine#onAmbientModeChanged}).
     *
     * @param watchMode the current {@link WatchMode}
     */
    protected void onWatchModeChanged(WatchMode watchMode) {
        LogUtils.LOGV(TAG, String.format("PeppyWatchFaceService.onWatchModeChanged: watchMode=%s",
                watchMode.name()));
        if (mWatchFaceDrawer != null) {
            mWatchFaceDrawer.onConfigChanged();
        }
    }

    /**
     * Called when the "Use 24-hour format" user setting is modified.
     */
    protected void on24HourFormatChanged(boolean is24HourFormat) {

    }

    protected void onTapCommand(@TapType int tapType, int x, int y, long eventTime) {
        LogUtils.LOGV(TAG, "PeppyWatchFaceService.onTapCommand: " + tapType);
    }

    public void invalidate() {
        mWatchFaceEngine.invalidate();
    }

    public void addIntKeyIfMissing(DataMap dataMap, String key, int value) {
        if (!dataMap.containsKey(key)) {
            dataMap.putInt(key, value);
        }
    }

    public void addFloatKeyIfMissing(DataMap dataMap, String key, float value) {
        if (!dataMap.containsKey(key)) {
            dataMap.putFloat(key, value);
        }
    }

    public void addStringKeyIfMissing(DataMap dataMap, String key, String value) {
        if (!dataMap.containsKey(key)) {
            dataMap.putString(key, value);
        }
    }

    public void addBooleanKeyIfMissing(DataMap dataMap, String key, boolean value) {
        if (!dataMap.containsKey(key)) {
            dataMap.putBoolean(key, value);
        }
    }

    @Override
    public CanvasWatchFaceService.Engine onCreateEngine() {
        LogUtils.LOGV(TAG, "PeppyWatchFaceService.onCreateEngine");

        return mWatchFaceEngine;
    }

    private class WatchFaceEngine extends CanvasWatchFaceService.Engine implements
            DataApi.DataListener, ResultCallback<DailyTotalResult>,
            GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
            BaseWatchFaceConfig, WatchFaceCustomization {
        static final int PHONE_BATTERY_LEVEL_UPDATE_RATE_MS = 600000;
        static final int MSG_UPDATE_PHONE_BATTERY_LEVEL = 0;

        final Handler mUpdatePhoneBatteryLevelHandler = new Handler() {
            @Override
            public void handleMessage(Message message) {
                switch (message.what) {
                    case MSG_UPDATE_PHONE_BATTERY_LEVEL:
                        LogUtils.LOGV(TAG, "updating phone battery level");
                        if (mPeerId != null) {
                            LogUtils.LOGD(TAG, "not null");
                            sendPhoneBatteryLevelRequest();
                        } else {
                            LogUtils.LOGD(TAG, "null");
                            Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(
                                    new ResultCallback<NodeApi.GetConnectedNodesResult>() {
                                        @Override
                                        public void onResult(@NonNull
                                                NodeApi.GetConnectedNodesResult
                                                getConnectedNodesResult) {
                                            if (getConnectedNodesResult.getNodes() != null) {
                                                mPeerId = WearableAPIHelper.pickBestNodeId(
                                                        getConnectedNodesResult.getNodes());
                                                sendPhoneBatteryLevelRequest();
                                            }
                                        }
                                    }
                            );
                        }
                        mUpdatePhoneBatteryLevelHandler.sendEmptyMessageDelayed(
                                MSG_UPDATE_PHONE_BATTERY_LEVEL,
                                PHONE_BATTERY_LEVEL_UPDATE_RATE_MS);
                        break;
                }
            }
        };
        private final ScheduledExecutorService mScheduledTimeUpdaterPool =
                Executors.newScheduledThreadPool(2);
        private final BroadcastReceiver mTimeZoneReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                invalidate();
            }
        };
        private final BroadcastReceiver mBatteryLevelReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                mWatchBatteryLevel = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
                invalidate();
            }
        };
        private final Runnable mTimeUpdater = new Runnable() {
            @Override
            public void run() {
                invalidate();
            }
        };
        private ScheduledFuture<?> mScheduledTimeUpdater;
        private boolean mRegisteredTimeZoneReceiver = false;
        private boolean mRegisteredBatteryReceiver = false;
        private boolean mMuteMode;

        // BaseWatchFaceConfig

        /**
         * Gets the latest {@link Calendar} that was updated the last time onCalendarChanged was
         * called.
         *
         * @return latest {@link Calendar}
         */
        @Override
        public final Calendar getCalendar() {
            return mLatestCalendar;
        }

        /**
         * Returns the current {@link WatchMode}.
         *
         * @return the current {@link WatchMode}.
         */
        @Override
        public final WatchMode getCurrentWatchMode() {
            WatchMode watchMode;
            if (mIsAmbient) {
                if (mBurnInProtection) {
                    if (mLowBitAmbient) {
                        watchMode = WatchMode.LOW_BIT_BURN_IN;
                    } else {
                        watchMode = WatchMode.BURN_IN;
                    }
                } else if (mLowBitAmbient) {
                    watchMode = WatchMode.LOW_BIT;
                } else {
                    watchMode = WatchMode.AMBIENT;
                }
            } else {
                watchMode = WatchMode.INTERACTIVE;
            }
            return watchMode;
        }

        /**
         * Returns current watch battery level.
         *
         * @return watch battery level.
         */
        @Override
        public final int getWatchBatteryLevel() {
            return mWatchBatteryLevel;
        }

        /**
         * Returns true if user preference is set to 24-hour format.
         *
         * @return true if 24 hour time format is selected, false otherwise.
         */
        @Override
        public final boolean is24HourFormat() {
            return mIs24HourFormat;
        }

        @Override
        public final Rect getPeekCardBounds() {
            return mPeekCardBounds;
        }

        /**
         * Returns the shape of the watch face (e.g. round, square)
         *
         * @return watch face {@link WatchShape}.
         */
        @Override
        public final WatchShape getWatchShape() {
            return mWatchShape;
        }

        @Override
        public int getChinSize() {
            return mChinSize;
        }

        @Override
        public int getPhoneBatteryLevel() {
            return mPhoneBatteryLevel;
        }

        /**
         * Returns total daily step count.
         *
         * @return daily step count.
         */
        @Override
        public final int getDailyStepCount() {
            return mStepsTotal;
        }

        @Override
        public void onCreate(SurfaceHolder holder) {
            LogUtils.LOGD(TAG, "onCreate");
            super.onCreate(holder);

            mStepsRequested = false;
            mGoogleApiClient = new GoogleApiClient.Builder(PeppyWatchFaceService.this)
                    .addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this)
                    .addApi(Wearable.API)
                    .addApi(Fitness.HISTORY_API)
                    .addApi(Fitness.RECORDING_API)
                    // When user has multiple accounts, useDefaultAccount() allows Google Fit to
                    // associated with the main account for steps. It also replaces the need for
                    // a scope request.
                    .useDefaultAccount()
                    .build();

            mPreviousCalendar = Calendar.getInstance();
            mLatestCalendar = Calendar.getInstance();
        }

        @Override
        public void onDestroy() {
            cancelTimeUpdater();
            mScheduledTimeUpdaterPool.shutdown();

            mUpdatePhoneBatteryLevelHandler.removeMessages(MSG_UPDATE_PHONE_BATTERY_LEVEL);

            super.onDestroy();
        }

        /**
         * Starts the {@link #mUpdatePhoneBatteryLevelHandler} timer if it should be running and
         * isn't currently or stops it if it shouldn't be running but currently is.
         */
        private void updateBatteryTimer() {
            LogUtils.LOGD(TAG, "updateBatteryTimer");
            mUpdatePhoneBatteryLevelHandler.removeMessages(MSG_UPDATE_PHONE_BATTERY_LEVEL);
            mUpdatePhoneBatteryLevelHandler.sendEmptyMessage(MSG_UPDATE_PHONE_BATTERY_LEVEL);
        }

        private void checkTimeUpdater() {
            checkTimeUpdater(getInteractiveModeUpdateRate(), true);
        }

        private void checkTimeUpdater(long updateRate, boolean delayStart) {
            cancelTimeUpdater();
            // Note that when we're ambient or invisible, we rely on timeTick to update instead
            // of a scheduled future
            if (!mIsAmbient && isVisible()) {
                // start updater on next second (millis = 0) when delayed start is requested
                long initialDelay = (delayStart ? DateUtils.SECOND_IN_MILLIS
                        - (System.currentTimeMillis() % 1000) : 0);
                mScheduledTimeUpdater = mScheduledTimeUpdaterPool.scheduleAtFixedRate(mTimeUpdater,
                        initialDelay, updateRate, TimeUnit.MILLISECONDS);
            }
        }

        private void cancelTimeUpdater() {
            if (mScheduledTimeUpdater != null) {
                mScheduledTimeUpdater.cancel(true);
            }
        }

        private boolean isTimeUpdaterRunning() {
            return (mScheduledTimeUpdater != null && !mScheduledTimeUpdater.isCancelled());
        }

        @Override
        public void onPropertiesChanged(Bundle properties) {
            super.onPropertiesChanged(properties);
            LogUtils.LOGD(TAG, "onPropertiesChanged: low-bit ambient = " + mLowBitAmbient);

            mLowBitAmbient = properties.getBoolean(PROPERTY_LOW_BIT_AMBIENT, false);
            mBurnInProtection = properties.getBoolean(PROPERTY_BURN_IN_PROTECTION, false);
        }

        @Override
        public void onTapCommand(@TapType int tapType, int x, int y, long eventTime) {
            super.onTapCommand(tapType, x, y, eventTime);
            PeppyWatchFaceService.this.onTapCommand(tapType, x, y, eventTime);
        }

        @Override
        public void onTimeTick() {
            super.onTimeTick();

            // only update if layout has completed and time updater not running
            if (mLayoutComplete && !isTimeUpdaterRunning()) {
                invalidate();
            }
        }

        @Override
        public void onAmbientModeChanged(boolean inAmbientMode) {
            super.onAmbientModeChanged(inAmbientMode);
            LogUtils.LOGD(TAG, "onConfigChanged: " + inAmbientMode);

            if (mWatchFaceDrawer != null && mIsAmbient != inAmbientMode) {
                mWatchFaceDrawer.onConfigChanged();
            }

            mIsAmbient = inAmbientMode;

            onWatchModeChanged(getCurrentWatchMode());
            invalidate();
            checkTimeUpdater();
        }

        @Override
        public void onInterruptionFilterChanged(int interruptionFilter) {
            super.onInterruptionFilterChanged(interruptionFilter);
            boolean inMuteMode = (interruptionFilter == WatchFaceService.INTERRUPTION_FILTER_NONE);

            /* Dim display in mute mode. */
            if (mMuteMode != inMuteMode) {
                mMuteMode = inMuteMode;
            }
        }

        @Override
        public void onSurfaceChanged(SurfaceHolder holder, int format, int width, int height) {
            super.onSurfaceChanged(holder, format, width, height);

            if (mWidth != width || mHeight != height) {
                mWidth = width;
                mHeight = height;

                if (mWatchFaceDrawer != null) {
                    mWatchFaceDrawer.setSize(getWidth(), getHeight());
                }

                if (mLayoutComplete) {
                    // A size change has occurred after the first layout. The subclass must
                    // re-layout.
                    onLayout(mWatchShape, mWidth, mHeight, mFaceInsets);
                } // else, we wait for onApplyWindowInsets to perform the first layout.
            }
        }

        @Override
        public void onDraw(Canvas canvas, Rect bounds) {
            super.onDraw(canvas, bounds);

            getTotalSteps();

            mPreviousCalendar.setTimeInMillis(mLatestCalendar.getTimeInMillis());
            long now = System.currentTimeMillis();
            mLatestCalendar.setTimeInMillis(now);
            mLatestCalendar.setTimeZone(TimeZone.getDefault());

            onCalendarChanged(mPreviousCalendar, mLatestCalendar);

            boolean is24Hour = DateFormat.is24HourFormat(PeppyWatchFaceService.this);
            if (is24Hour != mIs24HourFormat) {
                mIs24HourFormat = is24Hour;
                on24HourFormatChanged(mIs24HourFormat);
            }

            if (canvas == null) {
                LogUtils.LOGD(TAG,
                        "Cannot execute invalidate - WatchFaceService WatchFaceEngine Canvas is " +
                                "null.");
                return;
            }

            try {
                if (mWatchFaceDrawer != null) {
                    mWatchFaceDrawer.onDraw(canvas);
                }
            } catch (Exception e) {
                LogUtils.LOGE(TAG, "Exception in PeppyWatchFaceService onDraw", e);
            }
        }

        private void sendPhoneBatteryLevelRequest() {
            Wearable.MessageApi.sendMessage(mGoogleApiClient,
                    mPeerId,
                    Constants.PATH_REQUEST_PHONE_BATTERY_LEVEL,
                    new byte[0]);

            LogUtils.LOGD(TAG, "Sent watch face phone battery level request message.");
        }

        @Override
        public void onVisibilityChanged(boolean visible) {
            super.onVisibilityChanged(visible);

            if (mVisible != visible) {
                mVisible = visible;
            }

            if (mVisible) {
                mGoogleApiClient.connect();

                registerReceiver();
                invalidate();
            } else {
                unregisterReceiver();

                if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
                    Wearable.DataApi.removeListener(mGoogleApiClient, WatchFaceEngine.this);
                    mGoogleApiClient.disconnect();
                }

            }

            checkTimeUpdater();
        }

        @Override
        public void onPeekCardPositionUpdate(Rect rect) {
            super.onPeekCardPositionUpdate(rect);
            mPeekCardBounds.set(rect);
        }

        private void registerReceiver() {
            if (!mRegisteredTimeZoneReceiver) {
                mRegisteredTimeZoneReceiver = true;
                IntentFilter timeZonefilter = new IntentFilter(Intent.ACTION_TIMEZONE_CHANGED);
                PeppyWatchFaceService.this.registerReceiver(mTimeZoneReceiver, timeZonefilter);
            }
            if (!mRegisteredBatteryReceiver) {
                IntentFilter batteryFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
                PeppyWatchFaceService.this.registerReceiver(mBatteryLevelReceiver, batteryFilter);
            }
        }

        private void unregisterReceiver() {
            if (mRegisteredTimeZoneReceiver) {
                mRegisteredTimeZoneReceiver = false;
                PeppyWatchFaceService.this.unregisterReceiver(mTimeZoneReceiver);
            }
            if (mRegisteredBatteryReceiver) {
                mRegisteredBatteryReceiver = false;
                PeppyWatchFaceService.this.unregisterReceiver(mBatteryLevelReceiver);
            }
        }

        @Override
        public void onApplyWindowInsets(WindowInsets insets) {
            LogUtils.LOGD(TAG, "onApplyWindowInsets: " + (insets.isRound() ? "round" : "square"));
            super.onApplyWindowInsets(insets);

            mFaceInsets = insets;
            mWatchShape = mFaceInsets.isRound() ? WatchShape.CIRCLE : WatchShape.SQUARE;

            WatchFaceStyle watchFaceStyle = getWatchFaceStyle();
            if (watchFaceStyle != null) {
                setWatchFaceStyle(watchFaceStyle);
            }

            onLayout(mWatchShape, mWidth, mHeight, mFaceInsets);

            // Start the time updater after the first layout is complete.
            if (!mLayoutComplete) {
                mLayoutComplete = true;
                onLayoutCompleted();
                invalidate();
                checkTimeUpdater();
            }
        }

        @Override
        public void onDataChanged(DataEventBuffer dataEvents) { // DataApi.DataListener
            for (DataEvent dataEvent : dataEvents) {
                if (dataEvent.getType() != DataEvent.TYPE_CHANGED) {
                    continue;
                }

                DataItem dataItem = dataEvent.getDataItem();
                String path = dataItem.getUri().getPath();
                if (path.equals(Constants.PATH_CONFIG)) {
                    DataMapItem dataMapItem = DataMapItem.fromDataItem(dataItem);
                    DataMap config = dataMapItem.getDataMap();
                    LogUtils.LOGD(TAG, "Config DataItem updated:" + config);
                    onWatchFaceConfigChanged(config);
                } else if (path.equals(Constants.PATH_REQUEST_PHONE_BATTERY_LEVEL)) {
                    DataMapItem dataMapItem = DataMapItem.fromDataItem(dataItem);
                    DataMap phoneBatteryLevel = dataMapItem.getDataMap();
                    LogUtils.LOGD(TAG, "Phone Battery Level DataItem updated:" + phoneBatteryLevel);
                    updatePhoneBatteryLevelForDataMap(phoneBatteryLevel);
                }

            }
        }

        private void getTotalSteps() {
            LogUtils.LOGD(TAG, "getTotalSteps()");

            if ((mGoogleApiClient != null)
                    && (mGoogleApiClient.isConnected())
                    && (!mStepsRequested)) {

                mStepsRequested = true;

                PendingResult<DailyTotalResult> stepsResult =
                        Fitness.HistoryApi.readDailyTotal(
                                mGoogleApiClient,
                                DataType.TYPE_STEP_COUNT_DELTA);

                stepsResult.setResultCallback(this);
            }
        }

        /*
         * Subscribes to step count (for phones that don't have Google Fit app).
         */
        private void subscribeToSteps() {
            Fitness.RecordingApi.subscribe(mGoogleApiClient, DataType.TYPE_STEP_COUNT_DELTA)
                                .setResultCallback(new ResultCallback<Status>() {
                                    @Override
                                    public void onResult(@NonNull Status status) {
                                        if (status.isSuccess()) {
                                            if (status.getStatusCode()
                                                    ==
                                                    FitnessStatusCodes.SUCCESS_ALREADY_SUBSCRIBED) {
                                                LogUtils.LOGI(TAG,
                                                        "Existing subscription for activity " +
                                                                "detected.");
                                            } else {
                                                LogUtils.LOGI(TAG, "Successfully subscribed!");
                                            }
                                        } else {
                                            LogUtils.LOGI(TAG, "There was a problem subscribing.");
                                        }
                                    }
                                });
        }

        @Override
        public void onResult(@NonNull DailyTotalResult dailyTotalResult) {
            LogUtils.LOGD(TAG, "mGoogleApiAndFitCallbacks.onResult(): " + dailyTotalResult);

            mStepsRequested = false;

            if (dailyTotalResult.getStatus().isSuccess() && dailyTotalResult.getTotal() != null) {

                List<DataPoint> points = dailyTotalResult.getTotal().getDataPoints();

                if (!points.isEmpty()) {
                    mStepsTotal = points.get(0).getValue(Field.FIELD_STEPS).asInt();
                    LogUtils.LOGD(TAG, "steps updated: " + mStepsTotal);
                }
            } else {
                LogUtils.LOGE(TAG,
                        "onResult() failed! " + dailyTotalResult.getStatus().getStatusMessage());
            }
        }

        private void updateDataItemsOnStartup() {
            WearableAPIHelper.fetchDataMap(Constants.PATH_CONFIG, mGoogleApiClient,
                    new WearableAPIHelper.FetchDataMapCallback() {
                        @Override
                        public void onDataMapFetched(DataMap startupConfig) {
                            // If the DataItem hasn't been created yet or some keys are missing,
                            // use the default values.
                            setDefaultValuesForMissingConfigKeys(startupConfig);
                            WearableAPIHelper.putDataItem(Constants.PATH_CONFIG,
                                    mGoogleApiClient,
                                    startupConfig);

                            onWatchFaceConfigChanged(startupConfig);
                        }
                    }
            );

            WearableAPIHelper
                    .fetchDataMap(Constants.PATH_REQUEST_PHONE_BATTERY_LEVEL, mGoogleApiClient,
                            new WearableAPIHelper.FetchDataMapCallback() {
                                @Override
                                public void onDataMapFetched(DataMap startupBatteryLevel) {
                                    // If the DataItem hasn't been created yet or some keys are
                                    // missing, use the
                                    // default values.
                                    addIntKeyIfMissing(startupBatteryLevel,
                                            Constants.KEY_PHONE_BATTERY_LEVEL,
                                            Constants.VALUE_DEFAULT_PHONE_BATTERY_LEVEL);
                                    WearableAPIHelper
                                            .putDataItem(Constants.PATH_REQUEST_PHONE_BATTERY_LEVEL,
                                                    mGoogleApiClient,
                                                    startupBatteryLevel);

                                    updatePhoneBatteryLevelForDataMap(startupBatteryLevel);
                                }
                            }
                    );
        }

        private void setDefaultValuesForMissingConfigKeys(DataMap config) {
            addStringKeyIfMissing(config,
                    WatchFaceUtil.KEY_AMBIENT_MODE,
                    WatchFaceUtil.AmbientMode.DEFAULT.name());
            addStringKeyIfMissing(config,
                    WatchFaceUtil.KEY_TIME_FORMAT,
                    WatchFaceUtil.TimeFormat.DEFAULT.name());
        }

        private void onWatchFaceConfigChanged(DataMap config){
            boolean uiUpdated = false;
            for (String configKey : config.keySet()) {
                if (!config.containsKey(configKey)) {
                    continue;
                }
                String value = config.getString(configKey);
                LogUtils.LOGD(TAG, "Found watch face config key: " + configKey + " -> " + value);
                if (updateUiForKey(configKey, value)) {
                    uiUpdated = true;
                    mWatchFaceDrawer.onConfigChanged();
                }
            }
            if (uiUpdated) {
                invalidate();
            }
        }

        /**
         * Updates the color of a UI item according to the given {@code configKey}. Does nothing if
         * {@code configKey} isn't recognized.
         *
         * @return whether UI has been updated
         */
        private boolean updateUiForKey(String configKey, String value) {
            switch (configKey) {
                case WatchFaceUtil.KEY_AMBIENT_MODE: {
                    mConfiguredAmbientMode = WatchFaceUtil.AmbientMode.valueOf(value);
                    break;
                }
                case WatchFaceUtil.KEY_TIME_FORMAT: {
                    mConfiguredTimeFormat = WatchFaceUtil.TimeFormat.valueOf(value);
                    break;
                }
                default: {
                    LogUtils.LOGW(TAG, "Ignoring unknown config key: " + configKey);
                    return false;
                }
            }

            return true;
        }

        @Override
        public WatchFaceUtil.AmbientMode getAmbientMode() {
            return mConfiguredAmbientMode;
        }

        @Override
        public WatchFaceUtil.TimeFormat getTimeFormat() {
            return mConfiguredTimeFormat;
        }

        private void updatePhoneBatteryLevelForDataMap(final DataMap phoneBatteryLevel) {
            if (!phoneBatteryLevel.containsKey(Constants.KEY_PHONE_BATTERY_LEVEL)) {
                return;
            }
            mPhoneBatteryLevel = phoneBatteryLevel.getInt(Constants.KEY_PHONE_BATTERY_LEVEL);
        }

        @Override  // GoogleApiClient.ConnectionCallbacks
        public void onConnected(Bundle connectionHint) {
            LogUtils.LOGD(TAG, "onConnected: " + connectionHint);

            Wearable.DataApi.addListener(mGoogleApiClient, WatchFaceEngine.this);

            updateBatteryTimer();
            updateDataItemsOnStartup();

            mStepsRequested = false;

            // The subscribe step covers devices that do not have Google Fit installed.
            subscribeToSteps();

            getTotalSteps();
        }

        @Override  // GoogleApiClient.ConnectionCallbacks
        public void onConnectionSuspended(int cause) {
            LogUtils.LOGD(TAG, "onConnectionSuspended: " + cause);
        }

        @Override  // GoogleApiClient.OnConnectionFailedListener
        public void onConnectionFailed(@NonNull ConnectionResult result) {
            LogUtils.LOGD(TAG, "onConnectionFailed: " + result);
        }

    }
}
