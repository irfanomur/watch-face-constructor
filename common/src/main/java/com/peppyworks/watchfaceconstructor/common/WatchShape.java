/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.watchfaceconstructor.common;

/**
 * Enumeration of possible watch screen shapes.
 */

/**
 * Enumeration of possible watch screen shapes.
 * <p/>
 * <li>{@link #UNKNOWN}</li>
 * <li>{@link #SQUARE}</li>
 * <li>{@link #CIRCLE}</li>
 */
public enum WatchShape {
    /**
     * This is normally a placeholder until the watch face is informed by the system what shape it is.
     */
    UNKNOWN,
    SQUARE,
    CIRCLE
}
