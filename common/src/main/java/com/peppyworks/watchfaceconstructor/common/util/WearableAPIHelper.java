/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.watchfaceconstructor.common.util;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.PutDataMapRequest;
import com.google.android.gms.wearable.Wearable;

import android.net.Uri;
import android.support.annotation.NonNull;

import java.util.List;

public class WearableAPIHelper {
    private static final String TAG = LogUtils.makeLogTag("WearableAPIHelper");

    private WearableAPIHelper() {
    }

    /**
     * Asynchronously fetches the current {@link DataMap}  and passes it to the given callback.
     * <p>
     * If the current {@link DataItem} doesn't exist, it isn't created and the callback receives an
     * empty DataMap.
     */
    public static void fetchDataMap(final String path,
                                    final GoogleApiClient client,
                                    final FetchDataMapCallback callback) {
        Wearable.NodeApi.getLocalNode(client).setResultCallback(
                new ResultCallback<NodeApi.GetLocalNodeResult>() {
                    @Override
                    public void onResult(@NonNull NodeApi.GetLocalNodeResult getLocalNodeResult) {
                        if (getLocalNodeResult.getNode() != null) {
                            String localNode = getLocalNodeResult.getNode().getId();
                            Uri uri = new Uri.Builder()
                                    .scheme("wear")
                                    .path(path)
                                    .authority(localNode)
                                    .build();
                            Wearable.DataApi.getDataItem(client, uri)
                                    .setResultCallback(new DataItemResultCallback(callback));
                        }
                    }
                }
        );
    }

    /**
     * Overwrites (or sets, if not present) the keys in the current {@link DataItem} with the ones
     * appearing in the given {@link DataMap}. If the DataItem doesn't exist, it's created.
     * <p>
     * It is allowed that only some of the keys used in the DataItem appear in
     * {@code keysToOverwrite}. The rest of the keys remains unmodified in this case.
     */
    public static void overwriteKeysInDataMap(final String path,
                                              final GoogleApiClient googleApiClient,
                                              final DataMap keysToOverwrite) {
        fetchDataMap(path, googleApiClient,
                new FetchDataMapCallback() {
                    @Override
                    public void onDataMapFetched(DataMap currentDataMap) {
                        DataMap overwrittenDataMap = new DataMap();
                        overwrittenDataMap.putAll(currentDataMap);
                        overwrittenDataMap.putAll(keysToOverwrite);
                        putDataItem(path, googleApiClient, overwrittenDataMap);
                    }
                }
        );
    }

    /**
     * Overwrites the current {@link DataItem}'s {@link DataMap} with {@code newDataMap}.
     * If the DataItem doesn't exist, it's created.
     */
    public static void putDataItem(String path, GoogleApiClient googleApiClient, DataMap newDataMap) {
        PutDataMapRequest putDataMapRequest = PutDataMapRequest.create(path);
        putDataMapRequest.setUrgent();
        DataMap dataMapToPut = putDataMapRequest.getDataMap();
        dataMapToPut.putAll(newDataMap);
        Wearable.DataApi.putDataItem(googleApiClient, putDataMapRequest.asPutDataRequest())
                .setResultCallback(new ResultCallback<DataApi.DataItemResult>() {
                    @Override
                    public void onResult(@NonNull DataApi.DataItemResult dataItemResult) {
                        LogUtils.LOGD(TAG, "putDataItem result status: " + dataItemResult.getStatus());
                    }
                });
    }

    public static String pickBestNodeId(List<Node> nodes) {
        String bestNodeId = null;
        // Find a nearby node or pick one arbitrarily
        for (Node node : nodes) {
            if (node.isNearby()) {
                return node.getId();
            }
            bestNodeId = node.getId();
        }
        return bestNodeId;
    }

    /**
     * Callback interface to perform an action with the current {@link DataMap}
     */
    public interface FetchDataMapCallback {
        /**
         * Callback invoked with the current {@link DataMap}
         */
        void onDataMapFetched(DataMap dataMap);
    }

    private static class DataItemResultCallback implements ResultCallback<DataApi.DataItemResult> {

        private final FetchDataMapCallback mCallback;

        public DataItemResultCallback(FetchDataMapCallback callback) {
            mCallback = callback;
        }

        @Override
        public void onResult(@NonNull DataApi.DataItemResult dataItemResult) {
            if (dataItemResult.getStatus().isSuccess()) {
                if (dataItemResult.getDataItem() != null) {
                    DataItem dataItem = dataItemResult.getDataItem();
                    DataMapItem dataMapItem = DataMapItem.fromDataItem(dataItem);
                    DataMap dataMap = dataMapItem.getDataMap();
                    mCallback.onDataMapFetched(dataMap);
                } else {
                    mCallback.onDataMapFetched(new DataMap());
                }
            }
        }
    }
}
