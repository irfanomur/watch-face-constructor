/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.watchfaceconstructor.common.util;

public class WatchFaceUtil {

    /**
     * The ambient mode must be a {@link String}.
     */
    public static final String KEY_AMBIENT_MODE = "AMBIENT_MODE";
    public static final String KEY_TIME_FORMAT = "TIME_FORMAT";

    public enum AmbientMode {
        DEFAULT,
        BLACK_WHITE,
        FULL,
        TIME_ONLY,
        DIGITAL_TIME,
        ANALOG_TIME,
    }

    public enum TimeFormat {
        DEFAULT,
        FORMAT_12,
        FORMAT_12_WITH_ZERO,
        FORMAT_24,
        FORMAT_24_WITH_ZERO
    }

}
