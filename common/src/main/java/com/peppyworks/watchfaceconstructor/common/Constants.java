/*
 * Copyright (c) 2016 Peppy Works.
 */
package com.peppyworks.watchfaceconstructor.common;

import com.google.android.gms.wearable.DataItem;

public class Constants {
    /**
     * The path for the {@link DataItem}.
     */
    public static final String PATH_CONFIG = "/config";
    public static final String PATH_REQUEST_PHONE_BATTERY_LEVEL = "/request/phonebatterylevel";

    public static final String KEY_PHONE_BATTERY_LEVEL = "PHONE_BATTERY_LEVEL";
    public static final int VALUE_DEFAULT_PHONE_BATTERY_LEVEL = 0;
}
