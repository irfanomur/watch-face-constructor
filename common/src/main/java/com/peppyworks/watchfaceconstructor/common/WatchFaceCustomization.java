/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.watchfaceconstructor.common;

import com.peppyworks.watchfaceconstructor.common.util.WatchFaceUtil;

public interface WatchFaceCustomization {
    WatchFaceUtil.AmbientMode getAmbientMode();
    WatchFaceUtil.TimeFormat getTimeFormat();
}
