/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.watchfaceconstructor.common;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.graphics.PointF;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Region;
import android.graphics.Typeface;

import com.peppyworks.watchfaceconstructor.common.util.WatchFaceUtil;

import java.util.Calendar;
import java.util.Locale;

public abstract class BaseWatchFaceDrawer {
    /**
     * The design display size, in reference to which specs are created
     */
    public static final float SPEC_SIZE = 520.0f;

    private final Context mContext;
    private BaseWatchFaceConfig mBaseWatchFaceConfig;
    private WatchFaceCustomization mWatchFaceCustomization;

    private boolean mIsMobilePreview = false;
    private boolean mIsInitialised = false;
    private Path mPath;

    private int mWidth;
    private int mHeight;
    private int mRenderSize;

    // Specs scaled to current device dimensions
    private float mDigitalTimeTextSize;
    private float mDigitalTimeCenterCircleRadius;
    private PointF mDigitalTimeColonSize = new PointF(0f, 0f);
    private PointF mHourHandSize = new PointF(0f, 0f);
    private PointF mMinuteHandSize = new PointF(0f, 0f);

    private RectF mDigitalColonRect;
    private RectF mHourHandRect;
    private RectF mMinuteHandRect;

    /**
     * Paint used for all bitmaps
     */
    private Paint mBitmapPaint;

    private Paint mTimePaint;

    private Typeface mGothamNarrowLight;

    public BaseWatchFaceDrawer(Context context) {
        mContext = context;
        mGothamNarrowLight = Typeface.createFromAsset(mContext.getAssets(),
                mContext.getString(R.string.typeface_gotham_narrow_light));
    }

    public void setWatchFaceCustomization(WatchFaceCustomization watchFaceCustomization) {
        mWatchFaceCustomization = watchFaceCustomization;
    }

    public void setBaseWatchFaceConfig(BaseWatchFaceConfig baseWatchFaceConfig) {
        mBaseWatchFaceConfig = baseWatchFaceConfig;
    }

    public void setSize(int width, int height) {
        mWidth = width;
        mHeight = height;
        mRenderSize = Math.min(mWidth, mHeight);

        applyPointValueFromSpec(mHourHandSize, Spec.SPEC_mHourHandSize);
        applyPointValueFromSpec(mMinuteHandSize, Spec.SPEC_mMinuteHandSize);
        applyPointValueFromSpec(mDigitalTimeColonSize, Spec.SPEC_mDigitalTimeColonSize);
        mDigitalTimeCenterCircleRadius =
                getFloatValueFromSpec(Spec.SPEC_mDigitalTimeCenterCircleRadius);
        mDigitalTimeTextSize = getFloatValueFromSpec(Spec.SPEC_mDigitalTimeTextSize);

        onInitialised();
        mIsInitialised = true;
    }

    public boolean isMobilePreview() {
        return mIsMobilePreview;
    }

    public void setMobilePreview(boolean isMobilePreview) {
        mIsMobilePreview = isMobilePreview;
    }

    public boolean isInitialised() {
        return mIsInitialised;
    }

    public abstract void onInitialised();

    public void onConfigChanged() {
        WatchMode currentWatchMode = getCurrentWatchMode();
        WatchFaceUtil.AmbientMode configuredAmbientMode = mWatchFaceCustomization.getAmbientMode();
        if (mWatchFaceCustomization != null) {
            if (currentWatchMode == WatchMode.INTERACTIVE ||
                    configuredAmbientMode == WatchFaceUtil.AmbientMode.FULL) {
                applyFullState();
            } else if (configuredAmbientMode == WatchFaceUtil.AmbientMode.BLACK_WHITE ||
                    (configuredAmbientMode == WatchFaceUtil.AmbientMode.DEFAULT &&
                            currentWatchMode == WatchMode.AMBIENT)) {
                applyBWState();
            } else if (configuredAmbientMode == WatchFaceUtil.AmbientMode.TIME_ONLY) {
                applyTimeOnlyState();
            }
        }
    }

    public void clearConfig() {
    }

    public void applyFullState() {
    }

    public void applyBWState() {
    }

    public void applyTimeOnlyState() {
    }

    private String formatTwoDigitNumber(int hour) {
        return String.format(Locale.getDefault(), "%02d", hour);
    }

    public String getHourText() {
        int hour = getCalendar().get(Calendar.HOUR);
        if (hour == 0) {
            hour = 12;
        }
        switch (mWatchFaceCustomization.getTimeFormat()) {
            case DEFAULT: {
                if (is24HourFormat()) {
                    return formatTwoDigitNumber(getCalendar().get(Calendar.HOUR_OF_DAY));
                } else {
                    return formatTwoDigitNumber(hour);
                }
            }
            case FORMAT_12: {
                return String.valueOf(hour);
            }
            case FORMAT_12_WITH_ZERO: {
                return formatTwoDigitNumber(hour);
            }
            case FORMAT_24: {
                return String.valueOf(getCalendar().get(Calendar.HOUR_OF_DAY));
            }
            case FORMAT_24_WITH_ZERO: {
                return formatTwoDigitNumber(getCalendar().get(Calendar.HOUR_OF_DAY));
            }
            default:
                return "";
        }
    }

    public String getMinuteText() {
        return formatTwoDigitNumber(getCalendar().get(Calendar.MINUTE));
    }

    public String getSecondText() {
        return formatTwoDigitNumber(getCalendar().get(Calendar.SECOND));
    }

    public void onDraw(Canvas canvas) {
        if (isInitialised() && mIsMobilePreview && getWatchShape() == WatchShape.CIRCLE) {
            if (mPath == null) {
                mPath = new Path();
                mPath.addCircle(canvas.getWidth() / 2,
                        canvas.getHeight() / 2,
                        canvas.getWidth() / 2f,
                        Path.Direction.CW);
            }
            canvas.clipPath(mPath, Region.Op.INTERSECT);
        }

        if (getCurrentWatchMode() == WatchMode.INTERACTIVE) {
            onDrawFull(canvas);
        } else {
            if (mWatchFaceCustomization.getAmbientMode()
                                       .equals(WatchFaceUtil.AmbientMode.BLACK_WHITE)) {
                onDrawBW(canvas);
            } else if (mWatchFaceCustomization.getAmbientMode()
                                              .equals(WatchFaceUtil.AmbientMode.FULL)) {
                onDrawFull(canvas);
            } else if (mWatchFaceCustomization.getAmbientMode()
                                              .equals(WatchFaceUtil.AmbientMode.TIME_ONLY)) {
                onDrawTimeOnly(canvas);
            } else if (mWatchFaceCustomization.getAmbientMode().equals(
                    WatchFaceUtil.AmbientMode.DIGITAL_TIME)) {
                drawDigitalTime(canvas);
            } else if (mWatchFaceCustomization.getAmbientMode()
                                              .equals(WatchFaceUtil.AmbientMode.ANALOG_TIME)) {
                drawAnalogTime(canvas);
            }
        }
    }

    private void drawDigitalTime(Canvas canvas) {
        if (mTimePaint == null) {
            setTimePaint();
        }

        if (mDigitalColonRect == null) {
            mDigitalColonRect = new RectF((canvas.getWidth() - mDigitalTimeColonSize.x) / 2f,
                    (canvas.getHeight() - mDigitalTimeColonSize.y) / 2f,
                    (canvas.getWidth() + mDigitalTimeColonSize.x) / 2f,
                    (canvas.getHeight() + mDigitalTimeColonSize.y) / 2f);
        }

        canvas.drawColor(Color.BLACK);

        canvas.drawRoundRect(mDigitalColonRect, mDigitalTimeColonSize.x / 2f,
                mDigitalTimeColonSize.x / 2f, mTimePaint);

        String hourText = getHourText();
        canvas.drawText(hourText,
                canvas.getWidth() / 4f - mTimePaint.measureText(hourText) / 2f,
                (canvas.getHeight() - mTimePaint.descent() - mTimePaint.ascent()) / 2f,
                mTimePaint);

        String minuteText = formatTwoDigitNumber(getCalendar().get(Calendar.MINUTE));
        canvas.drawText(minuteText,
                3 * canvas.getWidth() / 4f - mTimePaint.measureText(minuteText) / 2f,
                (canvas.getHeight() - mTimePaint.descent() - mTimePaint.ascent()) / 2f,
                mTimePaint);
    }

    private void setTimePaint() {
        mTimePaint = new Paint();
        mTimePaint.setColor(Color.WHITE);
        mTimePaint.setAntiAlias(true);
        mTimePaint.setTextSize(mDigitalTimeTextSize);
        mTimePaint.setTypeface(mGothamNarrowLight);
    }

    private void drawAnalogTime(Canvas canvas) {
        if (mTimePaint == null) {
            setTimePaint();
        }

        canvas.drawColor(Color.BLACK);

        final float minutesRotation = getCalendar().get(Calendar.MINUTE) * 6f;

        final float hourHandOffset = getCalendar().get(Calendar.MINUTE) / 2f;
        final float hoursRotation = (getCalendar().get(Calendar.HOUR) * 30) + hourHandOffset;

        canvas.save();
        canvas.rotate(minutesRotation, canvas.getWidth() / 2f, canvas.getHeight() / 2f);

        if (mHourHandRect == null) {
            mHourHandRect = new RectF((canvas.getWidth() - mHourHandSize.x) / 2f,
                    (canvas.getHeight() + mHourHandSize.x) / 2f - mHourHandSize.y,
                    (canvas.getWidth() + mHourHandSize.x) / 2f,
                    (canvas.getHeight() + mHourHandSize.x) / 2f);
        }
        canvas.drawRoundRect(mHourHandRect, mHourHandSize.x / 2f, mHourHandSize.x / 2f, mTimePaint);

        canvas.rotate(hoursRotation - minutesRotation, canvas.getWidth() / 2f,
                canvas.getHeight() / 2f);
        if (mMinuteHandRect == null) {
            mMinuteHandRect = new RectF((canvas.getWidth() - mMinuteHandSize.x) / 2f,
                    (canvas.getHeight() + mMinuteHandSize.x) / 2f - mMinuteHandSize.y,
                    (canvas.getWidth() + mMinuteHandSize.x) / 2f,
                    (canvas.getHeight() + mMinuteHandSize.x) / 2f);
        }
        canvas.drawRoundRect(mMinuteHandRect, mHourHandSize.x / 2f, mHourHandSize.x / 2f,
                mTimePaint);

        canvas.restore();

        canvas.drawCircle(canvas.getWidth() / 2f, canvas.getHeight() / 2f,
                mDigitalTimeCenterCircleRadius, mTimePaint);

    }

    public abstract void onDrawBW(Canvas canvas);

    public abstract void onDrawFull(Canvas canvas);

    public abstract void onDrawTimeOnly(Canvas canvas);

    /**
     * Returns the design display size, in reference to which specs are created.
     * <br><br>DEFAULT={@link BaseWatchFaceDrawer#SPEC_SIZE}
     * <p>
     * * @return the design display size.
     */
    protected float getSpecSize() {
        return SPEC_SIZE;
    }

    /**
     * Scales a float dimension from a spec value to the specified screen size
     *
     * @param specValue The spec value to scale
     * @return The scaled dimension
     */
    public final float getFloatValueFromSpec(float specValue) {
        return (specValue / getSpecSize()) * mRenderSize;
    }

    /**
     * Scales a point value from a spec value to the specified screen size, and returns result in
     * output
     *
     * @param output    The point which will be set to the output of the operation
     * @param specValue The spec point to be scaled
     */
    public final void applyPointValueFromSpec(PointF output, PointF specValue) {
        if (output == null || specValue == null) {
            return;
        }
        output.set(getFloatValueFromSpec(specValue.x),
                getFloatValueFromSpec(specValue.y));
    }

    public final Bitmap getBitmap(int resID) {
        return scaleBitmap(BitmapFactory.decodeResource(mContext.getResources(), resID));
    }

    public final void drawBitmap(Canvas canvas, Bitmap bitmap, float left, float top) {
        if (bitmap == null) {
            return;
        }
        if (mBitmapPaint == null) {
            mBitmapPaint = new Paint();
            mBitmapPaint.setAntiAlias(true);
            mBitmapPaint.setFilterBitmap(true);
            mBitmapPaint.setDither(true);
        }

        canvas.drawBitmap(bitmap, left, top, mBitmapPaint);
    }

    /**
     * Scale individual bitmap inputs by creating a new bitmap according to the scale
     *
     * @param bitmap Original bitmap
     * @return Scaled bitmap
     */
    private Bitmap scaleBitmap(Bitmap bitmap) {
        PointF size = new PointF(getFloatValueFromSpec(bitmap.getWidth()),
                getFloatValueFromSpec(bitmap.getHeight()));
        return Bitmap.createScaledBitmap(bitmap, (int) size.x, (int) size.y, true);
    }

    public int getWidth() {
        return mWidth;
    }

    public int getHeight() {
        return mHeight;
    }

    public final Calendar getCalendar() {
        return mBaseWatchFaceConfig.getCalendar();
    }

    public final WatchMode getCurrentWatchMode() {
        return mBaseWatchFaceConfig.getCurrentWatchMode();
    }

    public final WatchShape getWatchShape() {
        return mBaseWatchFaceConfig.getWatchShape();
    }

    public final int getChinSize() {
        return mBaseWatchFaceConfig.getChinSize();
    }

    public final boolean is24HourFormat() {
        return mBaseWatchFaceConfig.is24HourFormat();
    }

    public final Rect getPeekCardBounds() {
        return mBaseWatchFaceConfig.getPeekCardBounds();
    }

    public final int getWatchBatteryLevel() {
        return mBaseWatchFaceConfig.getWatchBatteryLevel();
    }

    public final int getPhoneBatteryLevel() {
        return mBaseWatchFaceConfig.getPhoneBatteryLevel();
    }

    public final int getDailyStepCount() {
        return mBaseWatchFaceConfig.getDailyStepCount();
    }

    /**
     * Container class for design specifications
     */
    private static class Spec {
        static final PointF SPEC_mDigitalTimeColonSize = new PointF(5.0f, 28.0f);
        static final PointF SPEC_mHourHandSize = new PointF(9.0f, 131.0f);
        static final PointF SPEC_mMinuteHandSize = new PointF(5.0f, 204.0f);

        static final float SPEC_mDigitalTimeTextSize = 90.0f;
        static final float SPEC_mDigitalTimeCenterCircleRadius = 15.0f;
    }
}
