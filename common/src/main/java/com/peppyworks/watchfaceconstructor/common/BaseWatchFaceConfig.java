/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.watchfaceconstructor.common;

import android.graphics.Rect;

import java.util.Calendar;

public interface BaseWatchFaceConfig {
    Calendar getCalendar();
    WatchMode getCurrentWatchMode();
    WatchShape getWatchShape();
    int getChinSize();
    int getWatchBatteryLevel();
    int getPhoneBatteryLevel();
    int getDailyStepCount();
    boolean is24HourFormat();
    Rect getPeekCardBounds();
}
