/*
 * Copyright (c) 2016 Peppy Works.
 */
package com.peppyworks.watchfaceconstructor.mobile;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Path;
import android.graphics.Rect;
import android.graphics.RectF;
import android.text.format.DateFormat;
import android.util.AttributeSet;
import android.view.View;

import com.peppyworks.watchfaceconstructor.common.BaseWatchFaceConfig;
import com.peppyworks.watchfaceconstructor.common.BaseWatchFaceDrawer;
import com.peppyworks.watchfaceconstructor.common.WatchFaceCustomization;
import com.peppyworks.watchfaceconstructor.common.WatchMode;
import com.peppyworks.watchfaceconstructor.common.WatchShape;
import com.peppyworks.watchfaceconstructor.common.util.WatchFaceUtil;

import java.util.Calendar;
import java.util.TimeZone;

public class WatchFaceView extends View implements BaseWatchFaceConfig, WatchFaceCustomization {
    private static final int DEFAULT_WATCH_BATTERY_LEVEL = 100;
    private static final int DEFAULT_PHONE_BATTERY_LEVEL = 100;
    private static final int DEFAULT_DAILY_STEP_COUNT = 1234;

    private WatchFaceUtil.AmbientMode mAmbientMode = WatchFaceUtil.AmbientMode.DEFAULT;
    private WatchFaceUtil.TimeFormat mTimeFormat = WatchFaceUtil.TimeFormat.DEFAULT;

    private final Path mPath = new Path();
    private BaseWatchFaceDrawer mWatchFaceDrawer;
    private Calendar mCalendar;
    private WatchMode mCurrentWatchMode = WatchMode.INTERACTIVE;
    private WatchShape mWatchShape = WatchShape.CIRCLE;
    private int mChinSize;
    private int mWatchBatteryLevel = DEFAULT_WATCH_BATTERY_LEVEL;
    private int mPhoneBatteryLevel = DEFAULT_PHONE_BATTERY_LEVEL;
    private int mDailyStepCount = DEFAULT_DAILY_STEP_COUNT;

    private final Rect mPeekCardBounds = new Rect();

    public WatchFaceView(Context context) {
        super(context);
        init();
    }

    public WatchFaceView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    public WatchFaceView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    private void init() {
        setLayerType(View.LAYER_TYPE_SOFTWARE, null);

        mCalendar = Calendar.getInstance();
    }

    @Override
    public WatchFaceUtil.AmbientMode getAmbientMode() {
        return mAmbientMode;
    }

    @Override
    public WatchFaceUtil.TimeFormat getTimeFormat() {
        return mTimeFormat;
    }

    public void setAmbientMode(WatchFaceUtil.AmbientMode ambientMode) {
        mAmbientMode = ambientMode;
        update();
    }

    public void setTimeFormat(WatchFaceUtil.TimeFormat timeFormat) {
        mTimeFormat = timeFormat;
        update();
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);

        mWatchFaceDrawer.setSize(getMeasuredWidth(), getMeasuredHeight());
    }

    protected void setWatchFaceDrawer(BaseWatchFaceDrawer watchFaceDrawer) {
        mWatchFaceDrawer = watchFaceDrawer;
        mWatchFaceDrawer.setBaseWatchFaceConfig(this);
        mWatchFaceDrawer.setWatchFaceCustomization(this);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        mPath.reset();
        float round = 100;
        mPath.addRoundRect(new RectF(getPaddingLeft(),
                getPaddingTop(),
                w - getPaddingRight(),
                h - getPaddingBottom()), round, round, Path.Direction.CW);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        long now = System.currentTimeMillis();
        mCalendar.setTimeInMillis(now);
        mCalendar.setTimeZone(TimeZone.getDefault());

        if (mWatchFaceDrawer.isInitialised()) {
            mWatchFaceDrawer.onDraw(canvas);
        }
        invalidate();
    }

    protected void update() {
        if (mWatchFaceDrawer.isInitialised()) {
            mWatchFaceDrawer.onConfigChanged();
        }
    }

    @Override
    public Calendar getCalendar() {
        return mCalendar;
    }

    @Override
    public WatchMode getCurrentWatchMode() {
        return mCurrentWatchMode;
    }

    public void setCurrentWatchMode(WatchMode watchMode) {
        mCurrentWatchMode = watchMode;
        update();
    }

    @Override
    public WatchShape getWatchShape() {
        return mWatchShape;
    }

    public void setWatchShape(WatchShape watchShape) {
        mWatchFaceDrawer.clearConfig();
        mWatchShape = watchShape;
        update();
    }

    @Override
    public int getChinSize() {
        return mChinSize;
    }

    @Override
    public int getWatchBatteryLevel() {
        return mWatchBatteryLevel;
    }

    public void setWatchBatteryLevel(int watchBatteryLevel) {
        mWatchBatteryLevel = watchBatteryLevel;
    }

    @Override
    public int getPhoneBatteryLevel() {
        return mPhoneBatteryLevel;
    }

    public void setPhoneBatteryLevel(int phoneBatteryLevel) {
        mPhoneBatteryLevel = phoneBatteryLevel;
    }

    @Override
    public int getDailyStepCount() {
        return mDailyStepCount;
    }

    public void setDailyStepCount(int dailyStepCount) {
        mDailyStepCount = dailyStepCount;
    }

    @Override
    public boolean is24HourFormat() {
        return DateFormat.is24HourFormat(getContext());
    }

    @Override
    public Rect getPeekCardBounds() {
        return mPeekCardBounds;
    }
}
