/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.watchfaceconstructor.mobile.util;

import com.google.firebase.analytics.FirebaseAnalytics;

import com.peppyworks.watchfaceconstructor.common.util.LogUtils;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

/**
 * Centralized Analytics interface to ensure proper initialization and consistent analytics
 * application across the app.
 */
public class AnalyticsHelper {
    private static final String TAG = LogUtils.makeLogTag("AnalyticsHelper");
    public static final String EVENT_RATE = "rate";
    public static final String EVENT_CONTACT_US = "ContactUs";
    public static final String EVENT_CATEGORY_SOCIAL_MEDIA_TAPPED = "SocialMediaTapped";
    public static final String SOCIAL_MEDIA_TAPPED_FACEBOOK = "Facebook";
    public static final String SOCIAL_MEDIA_TAPPED_INSTAGRAM = "Instagram";
    public static final String EVENT_ITEM_WATCH_FACE = "WatchFace";

    private static FirebaseAnalytics mMeasurement;

    /**
     * Initialize the analytics tracker in use by the application. The {@code applicationContext}
     * parameter MUST be the application context or an object leak could occur.
     */
    public static synchronized void initializeAnalyticsTracker(Context applicationContext) {
        if (mMeasurement == null) {
            // Obtain the FirebaseAnalytics instance.
            mMeasurement = FirebaseAnalytics.getInstance(applicationContext);
        }
    }

    public static void recordCustomEvent(@NonNull String name,
                                         @NonNull String value) {
        if (isInitialized()) {
            LogUtils.LOGD(TAG, "recordCustomEvent:" + name + ":" + value);

            Bundle params = new Bundle();
            params.putString(FirebaseAnalytics.Param.VALUE, value);
            mMeasurement.logEvent(name, params);
        }
    }

    public static void recordCustomEvent(@NonNull String name) {
        if (isInitialized()) {
            LogUtils.LOGD(TAG, "recordCustomEvent:" + name);

            mMeasurement.logEvent(name, null);
        }

    }

    public static void recordItem(@NonNull String category,
                                  @Nullable String id,
                                  @NonNull String name) {
        if (isInitialized()) {
            LogUtils.LOGD(TAG, "recordItem:" + category + ":" + id + ":" + name);

            Bundle params = new Bundle();
            params.putString(FirebaseAnalytics.Param.ITEM_CATEGORY, category);
            if (id != null) {
                params.putString(FirebaseAnalytics.Param.ITEM_ID, id);
            }
            params.putString(FirebaseAnalytics.Param.ITEM_NAME, name);
            mMeasurement.logEvent(FirebaseAnalytics.Event.VIEW_ITEM, params);
        }
    }

    /**
     * Return the current initialization state which indicates whether events can be logged.
     */
    private static boolean isInitialized() {
        // Firebase Analytics is initialized when this class has a reference to an app context and
        // an measurement manager has been created.
        return mMeasurement != null; // Is there a measurement?
    }
}
