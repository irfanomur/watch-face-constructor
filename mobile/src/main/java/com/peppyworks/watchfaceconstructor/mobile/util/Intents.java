/*
 * Copyright (c) 2016 Peppy Works.
 */
package com.peppyworks.watchfaceconstructor.mobile.util;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

public class Intents {

    // Play store URL prefix
    public static final String PLAY_STORE_URL_PREFIX
            = "https://play.google.com/store/apps/details?id=";
    /**
     * URL for YouTube video IDs.
     */
    private static final String INSTAGRAM_URL = "http://instagram.com";
    private static final String INSTAGRAM_USERNAME = "peppy_works";
    public static final String FACEBOOK_URL = "https://www.facebook.com/PeppyWorks";

    /**
     * This allows the app to specify a {@code packageName} to handle the {@code intent}, if the
     * {@code packageName} is available on the device and can handle it. An example use is to open a
     * Google + stream directly using the Google + app.
     */
    public static void preferPackageForIntent(Context context, Intent intent, String packageName) {
        PackageManager pm = context.getPackageManager();
        if (pm != null) {
            for (ResolveInfo resolveInfo : pm.queryIntentActivities(intent, 0)) {
                if (resolveInfo.activityInfo.packageName.equals(packageName)) {
                    intent.setPackage(packageName);
                    break;
                }
            }
        }
    }

    public static void fireEmailIntent(Context context, String subject) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"support@peppyworks.com"});
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    /**
     * Constructs an Intent that open a Instagram profile. If the Instagram app is installed, the
     * profile page will be opened directly in full screen mode. if the Instagram app is not
     * available (e.g. not installed or disabled), the profile is launched in a browser instead.
     */
    public static void fireInstagramIntent(Context context) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(INSTAGRAM_URL + "/_u/" + INSTAGRAM_USERNAME));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        preferPackageForIntent(context, intent, "com.instagram.android");
        context.startActivity(intent);
    }

    /**
     * <p>Intent to open the official Facebook app. If the Facebook app is not installed then the
     * default web browser will be used.</p>
     *
     * @param context context
     */
    public static void fireFacebookIntent(Context context) {
        Uri uri = Uri.parse(FACEBOOK_URL);
        try {
            ApplicationInfo applicationInfo = context.getPackageManager().
                    getApplicationInfo("com.facebook.katana", 0);
            if (applicationInfo.enabled) {
                uri = Uri.parse("fb://facewebmodal/f?href=" + FACEBOOK_URL);
            }
        } catch (PackageManager.NameNotFoundException ignored) {
        }
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);
        if (intent.resolveActivity(context.getPackageManager()) != null) {
            context.startActivity(intent);
        }
    }

    public static void firePlayStoreIntent(Context context, String packageName) {
        Intent intent = new Intent(Intent.ACTION_VIEW,
                Uri.parse(PLAY_STORE_URL_PREFIX + packageName));
        preferPackageForIntent(context, intent, "com.android.vending");
        context.startActivity(intent);
    }

    public static boolean isAppInstalled(Context context, String uri) {
        PackageManager pm = context.getPackageManager();
        boolean installed;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            installed = true;
        } catch (PackageManager.NameNotFoundException e) {
            installed = false;
        }
        return installed;
    }
}
