/*
 * Copyright (c) 2016 Peppy Works.
 */
package com.peppyworks.watchfaceconstructor.mobile;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ScrollView;

public class SlidingScrollView extends ScrollView {

    public SlidingScrollView(Context context) {
        super(context);
    }

    public SlidingScrollView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public SlidingScrollView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    public void setXFraction(final float fraction) {
        float translationX = getWidth() * fraction;
        setTranslationX(translationX);
    }

    public float getXFraction() {
        if (getWidth() == 0) {
            return 0;
        }
        return getTranslationX() / getWidth();
    }
}
