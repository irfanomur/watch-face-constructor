/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.watchfaceconstructor.mobile;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.wearable.companion.WatchFaceCompanion;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataApi;
import com.google.android.gms.wearable.DataItem;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.DataMapItem;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.peppyworks.watchfaceconstructor.common.Constants;
import com.peppyworks.watchfaceconstructor.common.util.LogUtils;
import com.peppyworks.watchfaceconstructor.common.util.WearableAPIHelper;

public abstract class CompanionConfigActivity extends AppCompatActivity implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener,
        ResultCallback<DataApi.DataItemResult> {
    private static final String TAG = LogUtils.makeLogTag("CompanionConfigActivity");

    private GoogleApiClient mGoogleApiClient;
    private String mPeerId;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        mPeerId = getIntent().getStringExtra(WatchFaceCompanion.EXTRA_PEER_ID);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(Wearable.API)
                .build();
    }

    @Override
    protected void onStart() {
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    protected void onStop() {
        if (mGoogleApiClient != null && mGoogleApiClient.isConnected()) {
            mGoogleApiClient.disconnect();
        }
        super.onStop();
    }

    @Override // GoogleApiClient.ConnectionCallbacks
    public void onConnected(Bundle connectionHint) {
        LogUtils.LOGV(TAG, "onConnected: " + connectionHint);

        if (mPeerId != null) {
            fetchConfigDataMap();
        } else {
            Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).setResultCallback(
                    new ResultCallback<NodeApi.GetConnectedNodesResult>() {
                        @Override
                        public void onResult(
                                @NonNull NodeApi.GetConnectedNodesResult getConnectedNodesResult) {
                            if (getConnectedNodesResult.getNodes() != null) {
                                mPeerId = WearableAPIHelper.pickBestNodeId(
                                        getConnectedNodesResult.getNodes());
                                fetchConfigDataMap();
                            }
                        }
                    }
            );
        }
    }

    private void fetchConfigDataMap() {
        Uri.Builder builder = new Uri.Builder();
        Uri uri =
                builder.scheme("wear").path(Constants.PATH_CONFIG).authority(mPeerId).build();
        Wearable.DataApi.getDataItem(mGoogleApiClient, uri).setResultCallback(this);
    }

    @Override // ResultCallback<DataApi.DataItemResult>
    public void onResult(@NonNull DataApi.DataItemResult dataItemResult) {
        if (dataItemResult.getStatus().isSuccess() && dataItemResult.getDataItem() != null) {
            DataItem configDataItem = dataItemResult.getDataItem();
            DataMapItem dataMapItem = DataMapItem.fromDataItem(configDataItem);
            DataMap config = dataMapItem.getDataMap();
            onDataMapFetched(config);
        } else {
            // If DataItem with the current config can't be retrieved, select the default items on
            // each picker.
            onDataMapFetched(null);
        }
    }

    @Override // GoogleApiClient.ConnectionCallbacks
    public void onConnectionSuspended(int cause) {
        LogUtils.LOGD(TAG, "onConnectionSuspended: " + cause);
    }

    @Override // GoogleApiClient.OnConnectionFailedListener
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        LogUtils.LOGD(TAG, "onConnectionFailed: " + result);
    }

    protected abstract void onDataMapFetched(DataMap config);

    public void sendConfigUpdateMessage(String configKey, int value) {
        if (mPeerId != null) {
            DataMap config = new DataMap();
            config.putInt(configKey, value);
            byte[] rawData = config.toByteArray();
            Wearable.MessageApi.sendMessage(mGoogleApiClient,
                    mPeerId,
                    Constants.PATH_CONFIG,
                    rawData);

            LogUtils.LOGD(TAG, "Sent watch face config message: " + configKey + " -> "
                    + Integer.toHexString(value));
        }
    }

    public void sendConfigUpdateMessage(String configKey, boolean value) {
        if (mPeerId != null) {
            DataMap config = new DataMap();
            config.putBoolean(configKey, value);
            byte[] rawData = config.toByteArray();
            Wearable.MessageApi.sendMessage(mGoogleApiClient,
                    mPeerId,
                    Constants.PATH_CONFIG,
                    rawData);

            LogUtils.LOGD(TAG, "Sent watch face config message: " + configKey + " -> "
                    + Boolean.toString(value));
        }
    }

    public void sendConfigUpdateMessage(final String configKey, final String value) {
        if (mPeerId != null) {
            DataMap config = new DataMap();
            config.putString(configKey, value);
            byte[] rawData = config.toByteArray();
            Wearable.MessageApi.sendMessage(mGoogleApiClient,
                    mPeerId,
                    Constants.PATH_CONFIG,
                    rawData);

            LogUtils.LOGD(TAG, "Sent watch face config message: " + configKey + " -> " + value);
        }
    }

}
