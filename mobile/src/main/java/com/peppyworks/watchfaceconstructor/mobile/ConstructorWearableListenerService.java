/*
 * Copyright (c) 2016 Peppy Works.
 */

package com.peppyworks.watchfaceconstructor.mobile;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.BatteryManager;
import android.os.Bundle;
import android.support.annotation.NonNull;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.wearable.DataMap;
import com.google.android.gms.wearable.MessageApi;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;
import com.peppyworks.watchfaceconstructor.common.Constants;
import com.peppyworks.watchfaceconstructor.common.util.LogUtils;

import java.util.Collection;
import java.util.HashSet;
import java.util.concurrent.TimeUnit;

public class ConstructorWearableListenerService extends com.google.android.gms.wearable.WearableListenerService implements
        GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener {
    private static final String TAG = LogUtils.makeLogTag("WearableListenerService");
    private final BroadcastReceiver mBatteryLevelReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {

        }
    };
    private GoogleApiClient mGoogleApiClient;
    private boolean mRegisteredBatteryReceiver = false;

    @Override
    public void onCreate() {
        super.onCreate();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();

        registerReceiver();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        unregisterReceiver();
    }

    private void registerReceiver() {
        if (!mRegisteredBatteryReceiver) {
            mRegisteredBatteryReceiver = true;
            IntentFilter batteryFilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
            registerReceiver(mBatteryLevelReceiver, batteryFilter);
        }
    }

    private void unregisterReceiver() {
        if (mRegisteredBatteryReceiver) {
            unregisterReceiver(mBatteryLevelReceiver);
        }
    }

    @Override // ConstructorWearableListenerService
    public void onMessageReceived(MessageEvent messageEvent) {

        LogUtils.LOGD(TAG, "onMessageReceived: " + messageEvent);

        if (!messageEvent.getPath().equals(Constants.PATH_REQUEST_PHONE_BATTERY_LEVEL)) {
            return;
        }

        if (mGoogleApiClient == null) {
            mGoogleApiClient = new GoogleApiClient.Builder(this).addConnectionCallbacks(this)
                    .addOnConnectionFailedListener(this).addApi(Wearable.API).build();
        }
        if (!mGoogleApiClient.isConnected()) {
            ConnectionResult connectionResult =
                    mGoogleApiClient.blockingConnect(30, TimeUnit.SECONDS);

            if (!connectionResult.isSuccess()) {
                LogUtils.LOGE(TAG, "Failed to connect to GoogleApiClient.");
                return;
            }
        }
        IntentFilter ifilter = new IntentFilter(Intent.ACTION_BATTERY_CHANGED);
        Intent batteryStatus = registerReceiver(null, ifilter);
        if (batteryStatus != null) {
            int level = batteryStatus.getIntExtra(BatteryManager.EXTRA_LEVEL, -1);
            new SendPhoneBatteryLevelTask().execute(level);
        }

    }

    @Override // GoogleApiClient.ConnectionCallbacks
    public void onConnected(Bundle connectionHint) {
        LogUtils.LOGD(TAG, "onConnected: " + connectionHint);
    }

    @Override  // GoogleApiClient.ConnectionCallbacks
    public void onConnectionSuspended(int cause) {
        LogUtils.LOGD(TAG, "onConnectionSuspended: " + cause);
    }

    @Override  // GoogleApiClient.OnConnectionFailedListener
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        LogUtils.LOGD(TAG, "onConnectionFailed: " + result);
    }

    private void sendBatteryLevelMessage(String node, int level) {
        DataMap batteryLevel = new DataMap();
        batteryLevel.putInt(Constants.KEY_PHONE_BATTERY_LEVEL, level);
        byte[] rawData = batteryLevel.toByteArray();
        Wearable.MessageApi.sendMessage(
                mGoogleApiClient, node, Constants.PATH_REQUEST_PHONE_BATTERY_LEVEL, rawData)
                .setResultCallback(
                        new ResultCallback<MessageApi.SendMessageResult>() {
                            @Override
                            public void onResult(@NonNull
                                                 MessageApi.SendMessageResult sendMessageResult) {
                                if (!sendMessageResult.getStatus().isSuccess()) {
                                    LogUtils.LOGE(TAG, "Failed to send message with status code: "
                                            + sendMessageResult.getStatus().getStatusCode());
                                }
                            }
                        }
                );
    }

    private Collection<String> getNodes() {
        HashSet<String> results = new HashSet<>();
        NodeApi.GetConnectedNodesResult nodes =
                Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();

        for (Node node : nodes.getNodes()) {
            results.add(node.getId());
        }

        return results;
    }

    private class SendPhoneBatteryLevelTask extends AsyncTask<Integer, Void, Void> {

        @Override
        protected Void doInBackground(Integer... args) {
            Collection<String> nodes = getNodes();
            for (String node : nodes) {
                sendBatteryLevelMessage(node, args[0]);
            }
            return null;
        }
    }

}
